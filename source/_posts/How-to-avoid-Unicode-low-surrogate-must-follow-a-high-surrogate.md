---
title: How to avoid Unicode low surrogate must follow a high surrogate
date: 2020-04-11 13:39:53
tags: Django, Python, JavaScript, Unicode
---

# Issue description

## Main issue

最近前端送來了特殊字元讓後端發出 `Unicode low surrogate must follow a high surrogate.` 的錯誤，該錯誤來自，`🙏` 這個字被前端使用 JavaScript 內建的 `split` 函數，切割成 `\ud83d`和 `\ude4f`，後端在存入 PostgreSQL 時，產生錯誤

## Why the word be split to two characters

在原本的 Unicode UTF-8 中， 2003 前的版本僅支援 `U+0000` ~ `U+FFFF` (3 byte) ，`🙏` 其編碼是 2003 版本所支援的 `U+10000` ~ `U+10FFFF` (4 byte)， 在 JavaScript 的 `split("")` 是會將其分成兩個特殊字元 `\ud83d`和 `\ude4f`，這邊我們稱做 `surrogate pair` 分別為 `high surrogate`、`low surrogate`，以下編碼來自 [wiki](https://en.wikipedia.org/wiki/UTF-8#Description)

|Number of bytes|Bits for code point|First code point|Last code point|Byte 1  |Byte 2  |Byte 3  |Byte 4   |
|---------------|-------------------|----------------|---------------|--------|--------|--------|---------|
|1              |7                  |U+0000          |U+007F         |0xxxxxxx|        |        |         |
|2              |11                 |U+0080          |U+07FF         |110xxxxx|10xxxxxx|        |         |
|3              |16                 |U+0800          |U+FFFF         |1110xxxx|10xxxxxx|10xxxxxx|         |
|4              |21                 |U+10000         |U+10FFFF       |11110xxx|10xxxxxx|10xxxxxx|10xxxxxx |


# Fix it

## JavaScript 
### Split word with Array.from

在 JavaScript (ES5) 如果要將字元分割，但是又不想要把高於 U+FFFF 的字切成兩個字元的話，可以使用 `Array.from` 就可以拿到正常的字元陣列

```javascript
console.log("U+10000~: 🙏".split(""))
//["U", "+", "1", "0", "0", "0", "0", "~", ":", " ", "�", "�"]

console.log(Array.from("U+10000~: 🙏"))
// ["U", "+", "1", "0", "0", "0", "0", "~", ":", " ", "🙏"]
```

### Upgrade to ES6

ES6 好像已經解決該問題，所以升級就好了 (?)

## Python

在後端為了防範前端亂丟字元進來，我們可以將輸入的字串重新編碼，之後再做後續的處理，此處也可以用於需要傳送特殊字元到前端 (改寫 JSONRenderer)

```python
data = '\ud83d'
data = data.encode('utf-8', errors='backslashreplace').decode()
```

### PS
Python 在 3.3 以後支援到 `0x10FFFF` 所以在拿資料時就不會切開

```python
import sys


print(hex(sys.maxunicode))    # '0x10ffff'

[c for c in '🙏']    # ['🙏']
```

如果你的 `sys.maxunicode` 是 `0xffff` (小於 Python 3.3)，就會看到下方的樣子

```python
import sys


print(hex(sys.maxunicode))    # '0xffff'

[c for c in '🙏']    # ['\ud83d', '\ude4f']
```


# References
- [JavaScript Unicode](https://mathiasbynens.be/notes/javascript-unicode)
- [Wiki UTF-8](https://en.wikipedia.org/wiki/UTF-8)
- [How to work with surrogate pairs in Python](https://stackoverflow.com/questions/38147259/how-to-work-with-surrogate-pairs-in-python)
- [Get unicode code point of a character using Python](https://stackoverflow.com/a/42262842)

# Other keywords
- emoji
- Unicode low surrogate must follow a high surrogate
- UnicodeEncodeError: 'utf-8' codec can't encode character '\ud83d' in position : surrogates not allowed
